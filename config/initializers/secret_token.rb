# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RecipeBox::Application.config.secret_key_base = 'd3dca5b0806cff46a33d94fd17715fe59951df048d89c52355529f89875a5c52231b6fcb03d23089a37572b8b72b907419d931e022ea530083eb88141818cdd7'
